#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
import os
import sys
import argparse
import tempfile
from defs2bst import extract, Converter, timed_activity


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-y', '--ybd', required=True,
                        help='Path to ybd program')
    parser.add_argument('-d', '--definitions', required=True,
                        help='Path to the definitions repository to convert')
    parser.add_argument ('-o', '--output', required=True,
                         help="A directory for dumping the converted")
    parser.add_argument ('-r', '--rebase', required=True, nargs=2, metavar=('MORPH', 'BST'),
                         help="The old morph base target to replace with a new bst base target")
    parser.add_argument('target', help='The definitions directory relative YBD target')
    parser.add_argument('arch', help='The target arch')

    args = parser.parse_args()
    ybd = os.path.abspath(args.ybd)
    defsdir = os.path.abspath(args.definitions)
    outputdir = os.path.abspath(args.output)
    morph_base, bst_base = args.rebase

    with tempfile.TemporaryDirectory(prefix='defs2bst-') as workdir:

        # Invoke YBD and extract the YAML file we need
        try:
            yaml_file = extract(ybd, defsdir, os.path.abspath(workdir), args.target, args.arch)
        except Exception as e:
            print("\n  Error extracting target yml with YBD: {}\n".format(e))
            sys.exit(-1)

        # Update definitions based on the extracted YAML
        with timed_activity("Running conversion based on generated {}"
                            .format(os.path.basename(yaml_file))):
            converter = Converter(yaml_file, defsdir, outputdir, morph_base, bst_base)
            converter.convert()
