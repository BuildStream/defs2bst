#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
#        Pedro Alvarez <pedro.alvarez@codethink.co.uk>

import os
import re
from . import bst_dump

# Use buildstream internals
from buildstream import _yaml
from buildstream import buildelement

class DefKind():
    CHUNK = 'chunk'
    STRATUM = 'stratum'
    SYSTEM = 'system'

class BstKind():
    STACK = 'stack'
    IMPORT = 'import'
    MANUAL = 'manual'
    AUTOTOOLS = 'autotools'
    DISTUTILS = 'distutils'
    MAKEMAKER = 'makemaker'
    MODBUILD = 'modulebuild'
    CMAKE = 'cmake'
    QMAKE = 'qmake'

class BuildSys():
    MANUAL = 'manual'
    MANUAL_STRIP = 'manual-strip'
    AUTOTOOLS = 'autotools'
    PYTHON = 'python-distutils'
    PYTHON3 = 'python3-distutils'
    CPAN = 'cpan'
    MODBUILD = 'modulebuild'
    CMAKE = 'cmake'
    QMAKE = 'qmake'
    VERBATIM = 'verbatim'

class Source():
    REF = 'ref'
    SUBMOD = 'submodules'
    TRACK = 'track'
    URL = 'url'
    KIND = 'kind'
    GIT = 'git'

class Symbol():
    KIND = 'kind'
    DESC = 'description'
    CONTENTS = 'contents'
    BUILD_DEPENDS = 'build-depends'
    BUILDSYS = 'build-system'
    DEPENDS = 'depends'
    REPO = 'repo'
    REF = 'ref'
    SUBMOD = 'submodules'
    UNPETRIFY = 'unpetrify-ref'
    URL = 'url'
    MAX_JOBS = 'max-jobs'
    SYSTEM_INTEGRATION = 'system-integration'
    INTEGRATION = 'integration-commands'

class Element():
    KIND = 'kind'
    SOURCES = 'sources'
    DEPENDS = 'depends'
    TYPE = 'type'
    FILENAME = 'filename'
    CONFIG = 'config'
    VARS = 'variables'
    NOTPARALLEL = 'notparallel'
    ENV = 'environment'
    PUBLIC = 'public'
    BST = 'bst'


def node_items(node):
    for key, value in node.items():
        if key == _yaml.PROVENANCE_KEY:
            continue
        yield (key, value)


# The 'contents' of a stratum is expressed as:
#
# contents:
# - stata/foo: []
# - stata/bar: []
#
# This function returns 'stata/foo' from an element above
# so we can fetch the acutal declaration from it.
def content_list_extract_name(node):
    name = None
    for key, value in node_items(node):
        assert(name is None)
        name = key
    return name


# convert()
#
# Main conversion routine
#
# Args:
#    defs (str): Path to the <target>.yml extracted by ybd
#    defsdir (str): Directory of the definitions repo we're converting
#    directory (str): The output directory to output to
#    morph_base (str): The definitions relative path to the stratum base
#    bst_base (str): The output directory relative path to the new bst base
#
# Note: We assume that some manual work was done to provide a new base for
#       building the converted project on top of, morph_base and bst_base
#       are there to ensure that anything converted which depended on morph_base
#       will be made to depend on bst_base instead
#
class Converter():

    def __init__(self, defs, defsdir, directory, morph_base, bst_base):
        self.defs = _yaml.load(defs)
        self.defsdir = defsdir
        self.directory = directory

        # Internally we refer to paths without the .morph/.bst extensions
        self.morph_base = os.path.splitext(morph_base)[0]
        self.bst_base = os.path.splitext(bst_base)[0]

    def convert(self):

        os.makedirs(self.directory, exist_ok=True)

        # Loop over systems or strata, convert one stratum at a time, skipping the morph_base
        for key, value in node_items(self.defs):
            kind = _yaml.node_get(value, str, Symbol.KIND, default_value=DefKind.CHUNK)

            if (kind == DefKind.SYSTEM):
                self.convert_system(key, value)
            elif (kind == DefKind.STRATUM and key != self.morph_base):
                self.convert_stratum(key, value)

    # Creates a filename for the BuildStream output of a definitions file
    def output_name(self, name, suffix=None):

        # Do the rebasing here, we dont convert the original morph_base
        # and any reference to that base will be converted to the bst_base
        if name == self.morph_base:
            name = self.bst_base

        # Nuke the strata/ and systems/ subdirs
        name = re.sub("strata/", "", name)
        name = re.sub("systems/", "", name)

        if suffix is not None:
            name = name + suffix

        return name + '.bst'

    # Ensure the directory where we're going to dump something exists,
    # return the joined path while we're at it
    #
    def ensure_directory(self, name):
        fullpath = os.path.join(self.directory, name)
        fulldir = os.path.dirname(fullpath)
        os.makedirs(fulldir, exist_ok=True)
        return fullpath

    def convert_chunk_kind(self, element, chunk_node):
        need_python2 = False
        kind = _yaml.node_get(chunk_node, str, Symbol.BUILDSYS, default_value="manual")
        if kind == BuildSys.CPAN:
            kind = BstKind.MAKEMAKER
        if kind == BuildSys.MODBUILD:
            kind = BstKind.MODBUILD
        elif kind == BuildSys.MANUAL_STRIP:
            kind = BstKind.MANUAL
        elif kind == BuildSys.VERBATIM:
            kind = BstKind.IMPORT
        elif kind == BuildSys.PYTHON3:
            kind = BstKind.DISTUTILS
        elif kind == BuildSys.PYTHON:
            kind = BstKind.DISTUTILS
            need_python2 = True

        element[Element.KIND] = kind

        if need_python2:
            # Python (2) build system is just a distutils element
            # that uses python2 instead of default python3
            variables = element.get(Element.VARS, {})
            variables['python'] = 'python'
            element[Element.VARS] = variables

    def convert_chunk_depends(self, element, chunk_node):
        deps = _yaml.node_get(chunk_node, list, Symbol.BUILD_DEPENDS, default_value=[])
        element[Element.DEPENDS] = [self.output_name(dep) for dep in deps]

    def convert_chunk_max_jobs(self, element, chunk_node):
        if _yaml.node_get(chunk_node, int, Symbol.MAX_JOBS, default_value=0) == 1:
            variables = element.get(Element.VARS, {})
            variables[Element.NOTPARALLEL] = True
            element[Element.VARS] = variables

    def convert_chunk_sources(self, element, chunk_node):
        url = _yaml.node_get(chunk_node, str, Symbol.REPO, default_value='')
        if not url:
            return

        source = {}
        element[Element.SOURCES] = [source]

        source[Source.KIND] = Source.GIT
        source[Source.URL] = url
        source[Source.REF] = _yaml.node_get(chunk_node, str, Symbol.REF)

        unpetrify_ref = _yaml.node_get(chunk_node, str, Symbol.UNPETRIFY, default_value='')
        if unpetrify_ref:
            source[Source.TRACK] = unpetrify_ref

        submodules = _yaml.node_get(chunk_node, dict, Symbol.SUBMOD, default_value={})
        source_submodules = {}

        for name, submodule in node_items(submodules):
            url = _yaml.node_get(submodule, str, Symbol.URL)
            source_submodules[name] = {Source.URL: url}

        if source_submodules:
            source[Source.SUBMOD] = source_submodules

    def convert_chunk_description(self, element, chunk_node):
        description = _yaml.node_get(chunk_node, str, Symbol.DESC, default_value='')
        if description:
            element[Symbol.DESC] = str(description)

    # Helpers for chunk_convert_commands()
    def convert_commands(self, commands, need_destdir_env, need_prefix_env):
        converted_commands = []
        for line in commands:

            line = line.replace("$MORPH_ARCH", "%{arch}")
            line = line.replace("${MORPH_ARCH}", "%{arch}")
            line = line.replace("$TARGET", "$(gcc -dumpmachine)")
            line = line.replace("${TARGET}", "$(gcc -dumpmachine)")

            if not need_destdir_env:
                line = line.replace("$DESTDIR", "%{install-root}")
                line = line.replace("${DESTDIR}", "%{install-root}")

            if not need_prefix_env:
                line = line.replace("$PREFIX", "%{prefix}")
                line = line.replace("${PREFIX}", "%{prefix}")
                line = line.replace("${PREFIX-/usr}", "%{prefix}")

            # Special cases coming up !
            #
            if not need_destdir_env:

                # Special case .morph files which expect that DESTDIR is set
                # in the environment.
                #
                # This is needed for rsync, erlang and elixir, ofono, and
                # memcached. In some cases we could use %{make-install} here
                # instead of hardcoding a command, but erlang and elixir are
                # 'manual' elements which don't have %{make-install} defined.
                #
                # Note that zip morph also does this, but we work around that
                # by detecting the $$PREFIX & $$DESTDIR and setting those in
                # the environment instead.
                if line == 'make install':
                    line = 'make -j1 DESTDIR="%{install-root}" install'

                # krb5 and cracklib both omit DESTDIR from the install command line
                #
                if line == 'make -C src install':
                    line = 'make -j1 -C src DESTDIR="%{install-root}" install'

            converted_commands.append(line)

        return converted_commands

    def chunk_commands(self, node):
        need_destdir_env = False
        need_prefix_env = False
        for step in buildelement._command_steps:
            for prefix in ['pre-', '', 'post-']:
                commands = _yaml.node_get(node, list, prefix + step, default_value=[])
                if commands:
                    for line in commands:
                        if "$$PREFIX" in line:
                            need_prefix_env = True
                        if "$$DESTDIR" in line:
                            need_destdir_env = True

        config = {}
        for step in buildelement._command_steps:
            commands = _yaml.node_get(node, list, step, default_value=[])
            pre_commands = _yaml.node_get(node, list, 'pre-' + step, default_value=[])
            post_commands = _yaml.node_get(node, list, 'post-' + step, default_value=[])

            if commands:
                config[step] = self.convert_commands(pre_commands + commands + post_commands,
                                                     need_destdir_env, need_prefix_env)
            else:
                if pre_commands:
                    config[step] = {
                        '(<)': self.convert_commands(pre_commands, need_destdir_env, need_prefix_env)
                    }
                if post_commands:
                    config[step] = {
                        '(>)': self.convert_commands(post_commands, need_destdir_env, need_prefix_env)
                    }

        return config, need_destdir_env, need_prefix_env

    def convert_chunk_commands(self, element, chunk_node):
        commands, need_destdir_env, need_prefix_env = self.chunk_commands(chunk_node)
        if commands:
            element[Element.CONFIG] = commands

        if need_destdir_env or need_prefix_env:
            environment = element.get(Element.ENV, {})
            if need_destdir_env:
                environment['DESTDIR'] = '%{install-root}'
            if need_prefix_env:
                environment['PREFIX'] = '%{prefix}'
            element[Element.ENV] = environment

    def convert_chunk_integration(self, element, chunk_node):

        integration = _yaml.node_get(chunk_node, dict, Symbol.SYSTEM_INTEGRATION, default_value={})
        integration_commands = []

        # With baserock definitions, system-integration commands are
        # split into their respective artifact splits, here we get
        # a dictionary 'split' for every split name, which can be
        # something like 'linux-x86-32-generic-misc'
        for _, split in node_items(integration):

            # For each split, we have a dictionary which is meant
            # to represent a name and some commands to run under
            # that name, the name for each chunk is intended to use
            # as a sort key for running all the commands on a resulting
            # system.
            #
            # In practice the order does not matter this much, in
            # buildstream they will be run in staging order and we'll
            # retain the order in which they may have been specified
            # in the chunk morph for each generated element.
            #
            for _, commands in sorted(node_items(split)):
                integration_commands += commands

        if integration_commands:
            public = element.get(Element.PUBLIC, {})
            bst = public.get(Element.BST, {})

            bst[Symbol.INTEGRATION] = integration_commands

            public[Element.BST] = bst
            element[Element.PUBLIC] = public

    # A chunk gets converted into one of the existing build element types.
    #
    # Note that because of how YBD outputs the <target>.yml, the
    # individual chunks passed as 'chunk_node' already specify the dependencies
    # which they inherited from their containing stratum.
    #
    # Args:
    #    chunk_name: The name of the stratum to convert
    #    chunk_node: The dictionary for the given stratum found in `defs`
    #    directory: Project directory where to dump the converted element
    #
    def convert_chunk(self, chunk_name, chunk_node):
        outname = self.output_name(chunk_name)
        fullpath = self.ensure_directory(outname)

        print("Converting Chunk {} -> {}".format(chunk_name, fullpath))

        element = {}
        self.convert_chunk_kind(element, chunk_node)
        self.convert_chunk_depends(element, chunk_node)
        self.convert_chunk_max_jobs(element, chunk_node)
        self.convert_chunk_sources(element, chunk_node)
        self.convert_chunk_description(element, chunk_node)
        self.convert_chunk_commands(element, chunk_node)
        self.convert_chunk_integration(element, chunk_node)
        bst_dump(element, fullpath)

    # A stratum gets converted into a stack, the stack simply
    # depends on all the elements which were previously 'contained'
    # in the originating stratum.
    #
    # Args:
    #    defs: The loaded target.yml
    #    stratum_name: The name of the stratum to convert
    #    stratum_node: The dictionary for the given stratum found in `defs`
    #    directory: Project directory where to dump the converted stack
    #
    def convert_stratum(self, stratum_name, stratum_node):
        outname = self.output_name(stratum_name)
        fullpath = self.ensure_directory(outname)

        print("Converting Stratum: {} -> {}".format(stratum_name, outname))
        depends = []
        contents = _yaml.node_get(stratum_node, list, Symbol.CONTENTS, default_value=[])
        for thing in contents:
            chunkname = content_list_extract_name(thing)

            # Collect dependencies
            depends.append(self.output_name(chunkname))

            # Convert chunk, asserting that chunkname is found
            chunk = _yaml.node_get(self.defs, dict, chunkname)
            self.convert_chunk(chunkname, chunk)

        # Converting stacks to elements is not very difficult
        description = _yaml.node_get(stratum_node, str, Symbol.DESC, default_value='')
        stack = {}
        stack[Symbol.KIND] = BstKind.STACK

        # A stack just depends on all the elements which the strata 'contained'
        stack[Symbol.DEPENDS] = depends
        if description:
            stack[Symbol.DESC] = description

        bst_dump(stack, fullpath)

    # A system mostly gets converted into a stack just like a stratum would,
    # except for the supported configure extensions such as install-files
    # and such.
    #
    # Args:
    #    defs: The loaded target.yml
    #    system_name: The name of the system to convert
    #    system_node: The dictionary for the given system found in `defs`
    #    directory: Project directory where to dump the converted stack
    #
    def convert_system(self, system_name, system_node):
        content_outname = self.output_name(system_name, suffix="-content")
        system_outname = self.output_name(system_name)

        content_fullpath = self.ensure_directory(content_outname)
        system_fullpath = self.ensure_directory(system_outname)
        print("Converting System: {} -> {}".format(system_name, system_outname))

        # Collect dependencies
        depends = []
        contents = _yaml.node_get(system_node, list, Symbol.CONTENTS, default_value=[])
        for thing in contents:
            stratumname = content_list_extract_name(thing)
            depends.append(self.output_name(stratumname))

        # Generate and dump the stack
        content_stack = {}
        content_stack[Symbol.KIND] = BstKind.STACK
        content_stack[Symbol.DEPENDS] = depends
        description = _yaml.node_get(system_node, str, Symbol.DESC, default_value='')
        if description:
            content_stack[Symbol.DESC] = description

        bst_dump(content_stack, content_fullpath)

        # Now that we've converted the content of the system, create a
        # new stack which depends on that and additionally depends on
        # some elements we create to handle the configure extensions
        #
