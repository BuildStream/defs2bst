#!/usr/bin/env python3
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
import os
import subprocess
import datetime
from contextlib import contextmanager

timed_activity_depth = 0


class ProgramError(Exception):
    """Raised when running a program fails"""
    pass


def format_duration(elapsed):
    if elapsed is None:
        fields = ['--' for i in range(3)]
    else:
        hours, remainder = divmod(int(elapsed.total_seconds()), 60 * 60)
        minutes, seconds = divmod(remainder, 60)
        fields = [
            "{0:02d}".format(field) for field in [hours, minutes, seconds]
        ]
    return '[' + ':'.join(fields) + ']'


@contextmanager
def chdir(dirname=None):
    currentdir = os.getcwd()
    try:
        if dirname is not None:
            os.chdir(dirname)
        yield
    finally:
        os.chdir(currentdir)


@contextmanager
def timed_activity(message):
    global timed_activity_depth
    starttime = datetime.datetime.now()

    try:
        print('{} {}START: {}'.format(format_duration(None), ('  ' * timed_activity_depth), message))

        timed_activity_depth += 1
        yield

    except Exception as e:
        elapsed = datetime.datetime.now() - starttime
        timed_activity_depth -= 1

        print('{} {}FAIL: {}'.format(format_duration(elapsed), ('  ' * timed_activity_depth), message))
        raise e

    elapsed = datetime.datetime.now() - starttime
    timed_activity_depth -= 1
    print('{} {}SUCCESS: {}'.format(format_duration(elapsed), ('  ' * timed_activity_depth), message))


def run_program(*popenargs, **kwargs):
    """Run a program and collect stdout, stderr and exit code

    Because subprocess doesnt do this for us with a convenience wrapper
    """
    print("\n  Running command:\n    {}\n".format(' '.join(*popenargs)))

    kwargs['stdout'] = subprocess.PIPE
    kwargs['stderr'] = subprocess.PIPE
    proc = subprocess.Popen(*popenargs, **kwargs)
    outs, errs = proc.communicate()
    exit_code = proc.returncode

    if exit_code != 0:
        output = ''
        if outs:
            output += outs.decode('UTF-8')
        if errs:
            output += errs.decode('UTF-8')

        raise ProgramError("{} reported failure:\n\noutput: {}".format(popenargs[0], output))
